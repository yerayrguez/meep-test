package com.meep.meeptest

import android.app.Application
import com.meep.meeptest.datamanager.di.dataManagerProvider
import com.meep.meeptest.map.di.mapProvider
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MeepApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        setupInjection()
    }

    private fun setupInjection() {
        startKoin {
            androidContext(this@MeepApplication)
            modules(listOf(mapProvider, dataManagerProvider))
        }
    }
}