package com.meep.meeptest.datamanager.data.datasource

import com.meep.meeptest.datamanager.data.datasource.api.Endpoints
import com.meep.meeptest.datamanager.domain.model.PoiResource
import com.meep.meeptest.datamanager.domain.vo.LatLong

class ApiDataSource(
    private val endPoints: Endpoints
) {
    suspend fun getResources(lowerLeftLatLon: LatLong, upperRightLatLon: LatLong): List<PoiResource> {
        return endPoints.getResources(lowerLeftLatLon, upperRightLatLon)
    }
}