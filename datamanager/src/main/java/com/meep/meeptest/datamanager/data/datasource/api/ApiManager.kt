package com.meep.meeptest.datamanager.data.datasource.api

import com.meep.meeptest.datamanager.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiManager {

    private const val TIMEOUT: Long = 30

    fun createService(url: String): Retrofit {
        // Logging level
        val logging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }

        // Create OkHttpClient with timeout
        val httpClient = OkHttpClient.Builder()
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)

        // Set log interceptor
        httpClient.addInterceptor(logging)

        // Build OkHttpClient instance
        val client = httpClient.build()

        // Build retrofit instance
        val builder = Retrofit.Builder()
        builder.baseUrl(url)
        builder.addConverterFactory(GsonConverterFactory.create())
        builder.client(client)

        return builder.build()
    }
}