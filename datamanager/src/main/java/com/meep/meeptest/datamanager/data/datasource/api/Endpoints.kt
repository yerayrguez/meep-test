package com.meep.meeptest.datamanager.data.datasource.api

import com.meep.meeptest.datamanager.domain.model.PoiResource
import com.meep.meeptest.datamanager.domain.vo.LatLong
import retrofit2.http.GET
import retrofit2.http.Query

interface Endpoints {
    @GET("/tripplan/api/v1/routers/lisboa/resources")
    suspend fun getResources(
        @Query("lowerLeftLatLon") lowerLeftLatLon: LatLong,
        @Query("upperRightLatLon") upperRightLatLon: LatLong
    ): List<PoiResource>
}
