package com.meep.meeptest.datamanager.data.repository

import com.meep.meeptest.datamanager.data.datasource.ApiDataSource
import com.meep.meeptest.datamanager.domain.model.PoiResource
import com.meep.meeptest.datamanager.domain.vo.LatLong

class PoiRepository(
    private val apiDataSource: ApiDataSource
) {
    suspend fun fetchData(lowerLeftLatLon: LatLong, upperRightLatLon: LatLong): List<PoiResource> {
        return apiDataSource.getResources(lowerLeftLatLon, upperRightLatLon)
    }
}