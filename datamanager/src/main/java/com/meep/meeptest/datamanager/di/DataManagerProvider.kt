package com.meep.meeptest.datamanager.di

import com.meep.meeptest.datamanager.BuildConfig
import com.meep.meeptest.datamanager.data.datasource.ApiDataSource
import com.meep.meeptest.datamanager.data.datasource.api.ApiManager
import com.meep.meeptest.datamanager.data.datasource.api.Endpoints
import com.meep.meeptest.datamanager.data.repository.PoiRepository
import com.meep.meeptest.datamanager.domain.usecase.GetPoiUseCase
import org.koin.dsl.module

val dataManagerProvider = module {
    single {
        ApiManager
            .createService(BuildConfig.BASE_URL)
            .create(Endpoints::class.java)
    }
    single { GetPoiUseCase(get()) }
    single { PoiRepository(get()) }
    single { ApiDataSource(get()) }
}