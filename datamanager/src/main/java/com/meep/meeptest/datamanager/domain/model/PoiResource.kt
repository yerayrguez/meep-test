package com.meep.meeptest.datamanager.domain.model

import com.google.gson.annotations.SerializedName

data class PoiResource (
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("x") val x: Double,
    @SerializedName("y") val y: Double,
    @SerializedName("scheduledArrival") val scheduledArrival: Long? = null,
    @SerializedName("locationType") val locationType: Int? = null,
    @SerializedName("companyZoneId") val companyZoneId: Int? = null,
    @SerializedName("lat") val lat: Double? = null,
    @SerializedName("lon") val lon: Double? = null,
    @SerializedName("realTimeData") val realTimeData: Boolean? = null,
    @SerializedName("station") val station: Boolean? = null,
    @SerializedName("availableResources") val availableResources: Int? = null,
    @SerializedName("spacesAvailable") val spacesAvailable: Int? = null,
    @SerializedName("allowDropoff") val allowDropoff: Boolean? = null,
    @SerializedName("bikesAvailable") val bikesAvailable: Int? = null,
    @SerializedName("licencePlate") val licencePlate: String? = null,
    @SerializedName("range") val range: Int? = null,
    @SerializedName("batteryLevel") val batteryLevel: Int? = null,
    @SerializedName("seats") val seats: Int? = null,
    @SerializedName("model") val model: String? = null,
    @SerializedName("resourceImageId") val resourceImageId: String? = null,
    @SerializedName("resourceType") val resourceType: String? = null,
    @SerializedName("helmets") val helmets: Int? = null
)

/*
    {
        "id": "378:M28",
        "name": "ROSSIO",
        "x": -9.13796,
        "y": 38.71402,
        "scheduledArrival": 0,
        "locationType": 0,
        "companyZoneId": 378,
        "lat": 38.71402,
        "lon": -9.13796
    },
    {
        "id": "382:1_1401",
        "name": "IGREJA DE SÃO SEBASTIÃO",
        "x": -9.151616,
        "y": 38.731809,
        "scheduledArrival": 0,
        "locationType": 0,
        "companyZoneId": 382,
        "lat": 38.731809,
        "lon": -9.151616
    },
    {
        "id": "402:11059006",
        "name": "Rossio",
        "x": -9.1424,
        "y": 38.71497,
        "scheduledArrival": 0,
        "locationType": 0,
        "companyZoneId": 402,
        "lat": 38.71497,
        "lon": -9.1424
    },

    {
        "id": "304",
        "name": "304 - Avenida da Liberdade / São Jorge",
        "x": -9.14637,
        "y": 38.72026,
        "realTimeData": true,
        "station": true,
        "availableResources": 6,
        "spacesAvailable": 10,
        "allowDropoff": true,
        "companyZoneId": 412,
        "bikesAvailable": 6
    },
    {
        "id": "VR1URHNKKKW125895",
        "name": "166_32ZJ90",
        "x": -9.147218704223633,
        "y": 38.71603775024414,
        "licencePlate": "166_32ZJ90",
        "range": 440,
        "batteryLevel": 55,
        "seats": 5,
        "model": "null DS3",
        "resourceImageId": "vehicle_gen_emov",
        "realTimeData": true,
        "resourceType": "ELECTRIC_CAR",
        "companyZoneId": 467
    },
    {
        "id": "PT-LIS-A00289",
        "name": "34VQ51",
        "x": -9.149917,
        "y": 38.71492,
        "licencePlate": "34VQ51",
        "range": 32,
        "batteryLevel": 42,
        "helmets": 2,
        "model": "Askoll",
        "resourceImageId": "vehicle_gen_ecooltra",
        "realTimeData": true,
        "resourceType": "MOPED",
        "companyZoneId": 473
    },
 */