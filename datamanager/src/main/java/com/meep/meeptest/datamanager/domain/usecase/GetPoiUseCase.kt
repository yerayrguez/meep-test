package com.meep.meeptest.datamanager.domain.usecase

import com.meep.meeptest.datamanager.data.repository.PoiRepository
import com.meep.meeptest.datamanager.domain.model.PoiResource
import com.meep.meeptest.datamanager.domain.vo.LatLong
import com.meep.meeptest.datamanager.utils.Either
import com.meep.meeptest.datamanager.utils.rightIfNotNull

class GetPoiUseCase(
    private val poiRepository: PoiRepository
) {
    suspend fun execute(
        lowerLeftLatLon: LatLong?,
        upperRightLatLon: LatLong?
    ): Either<Throwable, List<PoiResource>> {
        return poiRepository.fetchData(lowerLeftLatLon!!, upperRightLatLon!!)
            .rightIfNotNull { Exception("Error fetching Pois") }
    }
}