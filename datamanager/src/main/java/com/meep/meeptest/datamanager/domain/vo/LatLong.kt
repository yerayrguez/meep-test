package com.meep.meeptest.datamanager.domain.vo

data class LatLong(
    val lat: Double,
    val lng: Double
) {
    override fun toString() = "${lat.toBigDecimal().toPlainString()},${lng.toBigDecimal().toPlainString()}"
}