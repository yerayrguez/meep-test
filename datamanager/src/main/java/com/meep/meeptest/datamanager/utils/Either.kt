package com.meep.meeptest.datamanager.utils

sealed class Either<out E, out V> {
    data class Left<out E>(val error: E) : Either<E, Nothing>()
    data class Right<out V>(val value: V) : Either<Nothing, V>()

    fun isRight(): Boolean = this is Right
    fun isLeft(): Boolean = this is Left

    companion object {
        fun <E, V> cond(
            predicate: Boolean,
            ifTrue: () -> V,
            ifFalse: () -> E
        ): Either<E, V> {
            return if (predicate) right(ifTrue()) else left(ifFalse())
        }
    }
}

fun <V> right(value: V): Either<Nothing, V> = Either.Right(value)
fun <E> left(value: E): Either<E, Nothing> = Either.Left(value)

inline fun <E, V, C> Either<E, V>.foldRight(block: (V) -> C): Either<E, C> {
    return when (this) {
        is Either.Right -> right(block(value))
        is Either.Left -> this
    }
}

inline fun <E, V, C> Either<E, V>.foldLeft(block: (E) -> C): Either<C, V> {
    return when (this) {
        is Either.Right -> this
        is Either.Left -> left(block(error))
    }
}

inline fun <E, V> Either<E, V>.fold(ifLeft: (E) -> Unit, ifRight: (V) -> Unit) {
    when (this) {
        is Either.Right -> ifRight(value)
        is Either.Left -> ifLeft(error)
    }
}

inline fun <E, V, M> Either<E, V>.map(func: (V) -> M): Either<E, M> {
    return when (this) {
        is Either.Right -> right(func(value))
        is Either.Left -> this
    }
}

inline fun <E, T, V> Either<E, V>.mapLeft(func: (E) -> T): Either<T, V> {
    return when (this) {
        is Either.Right -> this
        is Either.Left -> left(func(error))
    }
}

inline fun <E, V, T> Either<E, V>.flatMap(func: (V) -> Either<E, T>): Either<E, T> {
    return when (this) {
        is Either.Right -> func(this.value)
        is Either.Left -> this
    }
}

inline fun <E, V> V?.rightIfNotNull(block: () -> E): Either<E, V> {
    return this?.let { right(it) } ?: left(block())
}

inline fun <E, V> Either<E, V?>.leftIfNull(block: () -> E): Either<E, V> {
    return flatMap { value ->
        value?.let { right(it) } ?: left(block())
    }
}

fun <T> Result<T>.toEither(): Either<Throwable, T> = map { right(it) }.getOrElse { left(it) }

fun <E, V> Either<E, V>.rightOrElse(ifLeft: V): V {
    return when (this) {
        is Either.Right -> value
        else -> ifLeft
    }
}