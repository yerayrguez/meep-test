package com.meep.meeptest.map.di

import com.meep.meeptest.map.viewmodel.MapViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mapProvider = module {
    viewModel { MapViewModel(get()) }
}