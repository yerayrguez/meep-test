package com.meep.meeptest.map.ui

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.meep.meeptest.map.R
import com.meep.meeptest.map.databinding.ActivityMapBinding
import com.meep.meeptest.map.utils.getMarkerIcon
import com.meep.meeptest.map.viewmodel.MapViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MapActivity : AppCompatActivity() {

    private val mapViewModel: MapViewModel by viewModel()
    private lateinit var viewBinding: ActivityMapBinding
    private lateinit var googleMap: GoogleMap
    private lateinit var sheetBehavior: BottomSheetBehavior<*>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewBinding = ActivityMapBinding.inflate(LayoutInflater.from(this))
        setContentView(viewBinding.root)

        sheetBehavior = BottomSheetBehavior.from(viewBinding.include.bottomSheet)
        sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        with(viewBinding.map) {
            onCreate(null)
            getMapAsync {
                googleMap = it
                prepareMap()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewBinding.map.onResume()
    }

    override fun onPause() {
        super.onPause()
        viewBinding.map.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewBinding.map.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        viewBinding.map.onLowMemory()
    }

    private fun prepareMap() {
        googleMap.setOnMapLoadedCallback {
            googleMap.moveCamera(
                CameraUpdateFactory.newLatLngBounds(mapViewModel.visibleArea.value, 30))
            observeMap()
        }
    }

    private fun observeMap() {
        googleMap.setOnCameraIdleListener {
            mapViewModel.updateVisibleArea(googleMap.projection.visibleRegion)
        }

        mapViewModel.vehicles.observe(this, Observer { vehicles ->
            googleMap.clear()
            vehicles.forEach { resource ->
                val options = MarkerOptions()
                options.title(resource.id)
                options.position(LatLng(resource.y, resource.x))
                options.icon(BitmapDescriptorFactory.fromBitmap(
                    generateSmallIcon(resource.getMarkerIcon())))

                googleMap.addMarker(options)
            }
        })

        googleMap.setOnMarkerClickListener { marker ->
            Log.i("Marker", "marker clicked = " + marker.title)
            showVehicleInformation(marker.title)
            true
        }

        googleMap.setOnMapClickListener {
            if (sheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED)
                sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    private fun showVehicleInformation(vehicleId: String) {
        mapViewModel.getVehicleById(vehicleId)?.let { vehicle ->
            viewBinding.include.vehicleName.text = vehicle.name

            vehicle.batteryLevel?.let {
                viewBinding.include.tvBatteryLevel.visibility = View.VISIBLE
                viewBinding.include.tvBatteryLevel.text = String.format("%s%%", vehicle.batteryLevel)
            } ?: run { viewBinding.include.tvBatteryLevel.visibility = View.GONE }

            vehicle.licencePlate?.let {
                viewBinding.include.tvLicense.visibility = View.VISIBLE
                viewBinding.include.tvLicense.text = vehicle.licencePlate
            } ?: run { viewBinding.include.tvLicense.visibility = View.GONE }

            vehicle.helmets?.let {
                viewBinding.include.tvHelmets.visibility = View.VISIBLE
                viewBinding.include.tvHelmets.text = vehicle.helmets.toString()
            } ?: run { viewBinding.include.tvHelmets.visibility = View.GONE }

            vehicle.seats?.let {
                viewBinding.include.tvtvSeats.visibility = View.VISIBLE
                viewBinding.include.tvtvSeats.text = vehicle.seats.toString()
            } ?: run { viewBinding.include.tvtvSeats.visibility = View.GONE }

            vehicle.bikesAvailable?.let {
                viewBinding.include.tvBikes.visibility = View.VISIBLE
                viewBinding.include.tvBikes.text = vehicle.bikesAvailable.toString()
            } ?: run { viewBinding.include.tvBikes.visibility = View.GONE }

            sheetBehavior.isFitToContents = true
            sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    private fun generateSmallIcon(@DrawableRes resource: Int): Bitmap {
        val height = 75
        val width = 75
        val bitmap = BitmapFactory.decodeResource(resources, resource)
        return Bitmap.createScaledBitmap(bitmap, width, height, false)
    }
}
