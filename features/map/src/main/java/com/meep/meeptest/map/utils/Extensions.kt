package com.meep.meeptest.map.utils

import com.google.android.gms.maps.model.LatLng
import com.meep.meeptest.datamanager.domain.model.PoiResource
import com.meep.meeptest.datamanager.domain.vo.LatLong
import com.meep.meeptest.map.R

fun LatLng.getLegacyLatLng() = LatLong(this.latitude, this.longitude)

fun PoiResource.getMarkerIcon(): Int {
    return when(this.companyZoneId) {
        378 -> R.drawable.ic_marker_red
        382 -> R.drawable.ic_marker_blue
        402 -> R.drawable.ic_marker_dark
        412 -> R.drawable.ic_marker_orange
        467 -> R.drawable.ic_marker_green
        473 -> R.drawable.ic_marker_yellow
        else -> R.drawable.ic_marker_red
    }
}