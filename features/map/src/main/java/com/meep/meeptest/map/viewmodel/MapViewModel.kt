package com.meep.meeptest.map.viewmodel

import androidx.lifecycle.*
import androidx.lifecycle.Transformations.map
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.VisibleRegion
import com.meep.meeptest.datamanager.domain.model.PoiResource
import com.meep.meeptest.datamanager.domain.usecase.GetPoiUseCase
import com.meep.meeptest.datamanager.utils.map
import com.meep.meeptest.map.utils.getLegacyLatLng
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class MapViewModel(
    private val getPoiUseCase: GetPoiUseCase
): ViewModel() {

    private val initialNorthEast = LatLng(38.739429,-9.137115)
    private val initialSouthWest = LatLng(38.711046,-9.160096)

    val visibleArea = MutableLiveData<LatLngBounds>(LatLngBounds(initialSouthWest, initialNorthEast))
    val vehicles = map(visibleArea) { bounds ->
        getVehicles(bounds)
    }

    fun updateVisibleArea(visibleRegion: VisibleRegion?) {
        visibleRegion?.let {
            visibleArea.value = it.latLngBounds
        }
    }

    private fun getVehicles(bounds: LatLngBounds): List<PoiResource> {
        val resourceList = mutableListOf<PoiResource>()
        runBlocking {
            val result = withContext(Dispatchers.IO) {
                getPoiUseCase.execute(
                    bounds.southwest?.getLegacyLatLng(),
                    bounds.northeast?.getLegacyLatLng()
                )
            }
            result.map {
                resourceList.addAll(it)
            }
        }
        return resourceList
    }

    fun getVehicleById(id: String): PoiResource? {
        vehicles.value?.forEach { poi ->
            if (poi.id == id)
                return poi
        }
        return null
    }
}