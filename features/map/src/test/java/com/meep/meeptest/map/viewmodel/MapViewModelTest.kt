package com.meep.meeptest.map.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.VisibleRegion
import com.meep.meeptest.datamanager.domain.model.PoiResource
import com.meep.meeptest.datamanager.domain.usecase.GetPoiUseCase
import com.meep.meeptest.datamanager.utils.rightIfNotNull
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule


class MapViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val getPoiUseCase: GetPoiUseCase = mockk()
    private lateinit var viewModel: MapViewModel

    @Before
    fun setUp() {
        viewModel = MapViewModel(getPoiUseCase)
    }

    @Test
    fun `if visible area has been updated, should be update a list of items`() {
        coEvery { getPoiUseCase.execute(any(), any()) } returns mockListVehicles
            .rightIfNotNull { Exception() }

        val observer = Observer<List<PoiResource>>{}

        viewModel.updateVisibleArea(mockVisibleRegion)

        viewModel.vehicles.observeForever(observer)

        assertEquals(viewModel.vehicles.value, mockListVehicles)
    }

    @Test
    fun `if visible area has been updated could be return an empty list if the zone has 0 vehicles`() {
        coEvery { getPoiUseCase.execute(any(), any()) } returns emptyList<PoiResource>()
            .rightIfNotNull { Exception() }

        val observer = Observer<List<PoiResource>>{}

        viewModel.updateVisibleArea(mockVisibleRegion)

        viewModel.vehicles.observeForever(observer)

        assertEquals(viewModel.vehicles.value, emptyList<PoiResource>())
    }

    @Test
    fun `if the user click in a marker and exist in the list must return a valid resource`() {
        coEvery { getPoiUseCase.execute(any(), any()) } returns mockListVehicles
            .rightIfNotNull { Exception() }

        val observer = Observer<List<PoiResource>>{}

        viewModel.updateVisibleArea(mockVisibleRegion)

        viewModel.vehicles.observeForever(observer)

        assertTrue(viewModel.getVehicleById("1") != null)
    }

    @Test
    fun `if the user click in a marker but list has been updated must return null`() {
        coEvery { getPoiUseCase.execute(any(), any()) } returns mockListVehicles
            .rightIfNotNull { Exception() }

        val observer = Observer<List<PoiResource>>{}

        viewModel.updateVisibleArea(mockVisibleRegion)

        viewModel.vehicles.observeForever(observer)

        assertNull(viewModel.getVehicleById("2"))
    }

    companion object {
        val mockVisibleRegion = VisibleRegion(
            LatLng(38.71048692210361,-9.161496274173262),
            LatLng(38.71048692210361,-9.135714545845985),
            LatLng(38.73998726880886,-9.161496274173262),
            LatLng(38.73998726880886,-9.135714545845985),
            LatLngBounds(
                LatLng(38.71048692210361,-9.161496274173262),
                LatLng(38.73998726880886,-9.135714545845985)
            )
        )

        val mockListVehicles: List<PoiResource> = listOf(
            PoiResource(id = "1", name = "test", x = -9.149917, y = 38.71492)
        )
    }
}